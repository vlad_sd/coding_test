#include <stdio.h>
#include <stdlib.h>
#include <conio.h>

double func(double value, int degree)
{
    if (degree == 0)
    {
        return 1;
    }

    if (degree > 0)
    {
        return value *= func(value, degree - 1);
    }
   
    if (degree < 0)
    {
        
        return 1/func(value, -degree);
        
    }
   

}
int main()
{
    double value = 0;
    value = func(0.5, -3);
    printf("%f", value);
	_getch();
	return 0;
}